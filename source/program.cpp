// dear imgui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#include <stdlib.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <math.h>
#include <functional>

// About OpenGL function loaders: modern OpenGL doesn't have a standard header file and requires individual function pointers to be loaded manually. 
// Helper libraries are often used for this purpose! Here we are supporting a few common ones: gl3w, glew, glad.
// You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

#include <GLFW/glfw3.h> // Include glfw3.h after our OpenGL definitions

static void glfw_error_callback(int error, const char* description)
{
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

static const GLint swizzleMask[] = {GL_RED, GL_RED, GL_RED, 1};
//since input images r == g == b, can load just one subpixel and do computations faster
//however, for gui, must be rgb once again
//when loading one subpixel per pixel via glTexImage2D, use GL_RED
//https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml
//swizzlemask simply duplicates the "red"(actually intensity) across all channels to recreate the original greyscale image
/*
uploads a texture loaded from stbi_load or a 1d array of unsigned chars of intensity values to the GPU via OpenGL
@param texture is an array in which the generated texture names are stored. in this case, since only 1 texture is loaded, it can just be the address of an GLuint on the stack
@param length is the number of pixels on the x axis the generated texture and the source is
@param width is the number of pixels on the y axis the generated texture and the source is
@param pixels is the source from stbi_load or a 1d array of unsigned chars of intensity values
*/
void uploadTexture(GLuint* texture, unsigned int length, unsigned int width, unsigned char* pixels)
{
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, length, width, 0, GL_RED, GL_UNSIGNED_BYTE, pixels);
}
//https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
/*
converts sRGB component to its linear counterpart
@param intensity is a number from 0 - 255(sRGB)
@return a number representing the intensity in a linear fashion from 0 - 1
*/
double standardToLinear(unsigned char intensity)
{
	double linear = intensity / 255.0;
	if(linear <= 0.04045)
	{
		return linear / 12.92;
	}
	else
	{
		return pow((linear + 0.055) / 1.055, 2.4);
	}
}
/*
converts a linear component to its sRGB counterpart
@param intensity is a number representing the intensity in a linear fashion from 0 - 1
@return a number representing the intensity in a non linear fashion from 0 - 255(sRGB)
*/
int linearToStandard(double intensity)
{
	if(intensity <= 0.0031308)
	{
		return (int) (intensity * 12.92 * 255);
	}
	else
	{
		return (int) ((1.055 * pow(intensity, 1 / 2.4) - .055) * 255);
	}
}

struct Pixel
{
	unsigned int x;
	unsigned int y;
	unsigned char standardIntensity;
	double linearIntensity;
};
/*
finds the true black target automatically
@param pixels is the array of pixels from a dark screen calibration from which to choose the true black target from
@param length is the number of pixels on the x axis the dark screen calibration image is
@param width is the number of pixels on the y axis the dark screen calibration image is
@param comp is a function to compare 2 pixel intensities. here, it's used to find the min or max TBT from a DSC
@return a Pixel representing where and what the true black target is
*/
template <typename Comparator>
Pixel findTBT(unsigned char* pixels, unsigned int length, unsigned int width, Comparator comp)
{
	struct Pixel extremumTBT = { 0, 0, *pixels, 0 };
	for(unsigned int y_cursor = 0; y_cursor < width; ++y_cursor)
	{
		for(unsigned int x_cursor = 0; x_cursor < length; ++x_cursor)
		{
			if(comp(*pixels, extremumTBT.standardIntensity))
			{
				extremumTBT = { x_cursor, y_cursor, *pixels, 0 };
			}
			++pixels;
		}
	}
	extremumTBT.linearIntensity = standardToLinear(extremumTBT.standardIntensity);
	return extremumTBT;
}
/*
converts an image's sRGB values to linear to speed up GUI operations
side effects: mutates destination
@param source is an array of pixels representing an image encoded with sRGB valuess(0 - 255 intensity)
@param destination is an array of pixels to be filled with linear values(0 - 1) representing the intensity derived from source
@param length is the number of pixels on the x axis the images are
@param width is the number of pixels on the y axis the images are
*/
void standardToLinearImage(unsigned char* source, double* destination, unsigned int length, unsigned int width)
{
	for(unsigned int y_cursor = 0; y_cursor < width; ++y_cursor)
	{
		for(unsigned int x_cursor = 0; x_cursor < length; ++x_cursor)
		{
			*destination = standardToLinear(*source);
			++source;
			++destination;
		}
	}
}
/*
creates an adjustment data structure, consisting of the length and the width of the structure followed by the linear differences between each pixel and a true black target from a dark sscreen calibration image
side effects: this writes the adjutment data structure to disk and mutates ADS
@param pixels is the array of pixels from a dark screen calibration
@param length is the number of pixels on the x axis the dark screen calibration image is
@param width is the number of pixels on the y axis the dark screen calibration image is
@param TBT is a Pixel representing where and what the true black target is
@param ADS is an array to be populated with the adjustment data structure
*/
void createADS(double* pixels, unsigned int length, unsigned int width, Pixel TBT, double* ADS)
{
	double* ADScursor = ADS;
	*ADScursor = length;
	++ADScursor;
	*ADScursor = width;
	++ADScursor;
	for(unsigned int y_cursor = 0; y_cursor < width; ++y_cursor)
	{
		for(unsigned int x_cursor = 0; x_cursor < length; ++x_cursor)
		{
			*ADScursor = *pixels - TBT.linearIntensity;
			++pixels;
			++ADScursor;
		}
	}
	FILE* ADSFile = fopen("ADS", "wb");
	fwrite(ADS, sizeof(double), length * width + 2, ADSFile);
	fclose(ADSFile);
}
/*
applies an adjustment data structure to an image to correct display uniformity issues
side effects: this writes the adjustsed image to disk and mutates destination
@param source is an array of pixels representing an unaltered image that is to be corrected by the adjustment data strcuture
@param destination is an array of pixels to be populated representing the altered image that is corrected by the adjustment data structure
@param length is the number of pixels on the x axis the images are
@param width is the number of pixels on the y axis the images are
@param ADS is the adjustment data structure
@return a number indicating success. It will return 1(failure) if there is a dimension mismatch between the adjustment data structure and the source/destination
*/
int applyADS(double* source, unsigned char* destination, unsigned int length, unsigned int width, double* ADS)
{
	if(length == ADS[0] && width == ADS[1])
	{
		ADS += 2;
		unsigned char* destinationCursor = destination;
		for(unsigned int y_cursor = 0; y_cursor < width; ++y_cursor)
		{
			for(unsigned int x_cursor = 0; x_cursor < length; ++x_cursor)
			{
				double difference = *source - *ADS;
				if(difference < 0)
				{
					*destinationCursor = 0;
				}
				else if(difference > 1)
				{
					*destinationCursor = 255;
				}
				else
				{
					*destinationCursor = linearToStandard(difference);
				}
				++source;
				++destinationCursor;
				++ADS;
			}
		}
		stbi_write_png("fixed.png", length, width, 1, destination, 0);
		return 0;
	}
	else
	{
		return 1;
	}
}

int main(int argc, char** argv)
{
	if(argc > 1)
	{
		std::greater<int> gt = std::greater<int>();
		std::less<int> lt = std::less<int>();
		int length, width;
		unsigned char* DSC = stbi_load(argv[1], &length, &width, 0, 1);
		double* DSClinear = (double*) malloc(length * width * sizeof(double));
		standardToLinearImage(DSC, DSClinear, length, width);
		if(argc == 3)
		{
			int length2, width2;
			unsigned char* testImage = stbi_load(argv[2], &length2, &width2, 0, 1);
			double* testImageLinear;
			if(testImage)
			{
				testImageLinear = (double*) malloc(length * width * sizeof(double));
				standardToLinearImage(testImage, testImageLinear, length, width);
				if(length2 == length && width2 == width)
				//GUI(DSC, test image) =>
				// buttons & info
				// DSC             test image
				// DSC adjusted    test image adjusted
				{
					unsigned char* DSCadjusted = (unsigned char*) malloc(length * width * sizeof(unsigned char));
					unsigned char* testImageAdjusted = (unsigned char*) malloc(length * width * sizeof(unsigned char));
					// Setup window
					glfwSetErrorCallback(glfw_error_callback);
					if(glfwInit())
					{
						const char* glsl_version = "#version 150";
						glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
						glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
						glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
					#if __APPLE__
						glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
					#endif
						// Create window with graphics context
						GLFWwindow* window = glfwCreateWindow(1920, 1080, "VueReal Black Level Correction", NULL, NULL);
						if(window)
						{
							glfwMakeContextCurrent(window);
							glfwSwapInterval(1);//vsync on
							// Initialize OpenGL loader
						#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
							bool err = gl3wInit() != 0;
						#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
							bool err = glewInit() != GLEW_OK;
						#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
							bool err = gladLoadGL() != 0;
						#endif
							if(err)
							{
								fprintf(stderr, "Failed to initialize OpenGL loader!\n");
								return 1;
							}
							else
							{
								// Setup Dear ImGui binding
								IMGUI_CHECKVERSION();
								ImGui::CreateContext();
								ImGuiIO& io = ImGui::GetIO(); (void)io;
								ImGui_ImplGlfw_InitForOpenGL(window, true);
								ImGui_ImplOpenGL3_Init(glsl_version);
								
								GLuint DSCtexture;
								uploadTexture(&DSCtexture, length, width, DSC);
								GLuint DSCadjustedTexture;
								GLuint testImageTexture;
								uploadTexture(&testImageTexture, length, width, testImage);
								GLuint testImageAdjustedTexture;

								Pixel TBT = { length + 1, width + 1, 0, 0 };//this initial TBT is out of bounds to indicate TBT has not been chosen by the user yet
								double* ADS = (double*) malloc((width * length + 2) * sizeof(double));
								//main GUI loop
								bool need2update = false;
								while(!glfwWindowShouldClose(window))
								{
									glfwPollEvents();
									// Start the Dear ImGui frame
									ImGui_ImplOpenGL3_NewFrame();
									ImGui_ImplGlfw_NewFrame();
									ImGui::NewFrame();
									ImGui::Begin("Info");
									if(ImGui::Button("Automatically find darkest True Black Target"))
									{
										TBT = findTBT(DSC, length, width, lt);
										need2update = true;
									}
									if(ImGui::Button("Automatically find brightest True Black Target"))
									{
										TBT = findTBT(DSC, length, width, gt);
										need2update = true;
									}
									if(ImGui::Button("Test Image - DSC"))
									{
										TBT = { length, width, 0, 0 };//this will make createADS subtract 0, effectively making applyADS "subtract" the DSC from the test image
										need2update = true;
									}
									if(TBT.x <= length && TBT.y <= width)
									{
										if(need2update)
										{
											createADS(DSClinear, length, width, TBT, ADS);
											applyADS(DSClinear, DSCadjusted, length, width, ADS);
											uploadTexture(&DSCadjustedTexture, length, width, DSCadjusted);
											applyADS(testImageLinear, testImageAdjusted, length, width, ADS);
											uploadTexture(&testImageAdjustedTexture, length, width, testImageAdjusted);
											need2update = false;
										}
										if(TBT.x < length && TBT.y < width)
										{
											ImGui::Text("x: %d, y: %d, intensity[sRGB 0 - 255]: %d", TBT.x, TBT.y, TBT.standardIntensity);
										}
										else
										{
											ImGui::Text("subtracted");
										}
										ImGui::End();
										ImGui::Begin("DSC Adjusted");
										ImGui::Image((void*)(intptr_t)DSCadjustedTexture, ImVec2(length, width));
										ImGui::End();
										ImGui::Begin("Test Image Adjusted");
										ImGui::Image((void*)(intptr_t)testImageAdjustedTexture, ImVec2(length, width));
									}
									ImGui::End();
									ImGui::Begin("DSC Original (Click me!)");
									ImVec2 pos = ImGui::GetCursorScreenPos();
									ImGui::Image((void*)(intptr_t)DSCtexture, ImVec2(length, width));
									if(ImGui::IsItemHovered() && io.MouseDown[0])
									{
										TBT.x = io.MousePos.x - pos.x;
										TBT.y = io.MousePos.y - pos.y;
										TBT.standardIntensity = DSC[TBT.x + TBT.y * length];
										TBT.linearIntensity = DSClinear[TBT.x + TBT.y * length];
										need2update = true;
									}
									ImGui::End();
									ImGui::Begin("Test Image Original");
									ImGui::Image((void*)(intptr_t)testImageTexture, ImVec2(length, width));
									ImGui::End();
									// Rendering
									ImGui::Render();
									int display_w, display_h;
									glfwMakeContextCurrent(window);
									glfwGetFramebufferSize(window, &display_w, &display_h);
									glViewport(0, 0, display_w, display_h);
									glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
									glClear(GL_COLOR_BUFFER_BIT);
									ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

									glfwMakeContextCurrent(window);
									glfwSwapBuffers(window);
								}
								free(ADS);
								// Cleanup
								ImGui_ImplOpenGL3_Shutdown();
								ImGui_ImplGlfw_Shutdown();
								ImGui::DestroyContext();

								glfwDestroyWindow(window);
								glfwTerminate();
							}
						}
						else
						{
							return 1;
						}
					}
					else
					{
						return 1;
					}
					free(DSCadjusted);
					free(testImageAdjusted);
				}
				else
				{
					fprintf(stderr, "DSC(%d * %d) and test image(%d * %d) dimensions mismatch\n", length, width, length2, width2);
					return 1;
				}
				stbi_image_free(testImage);
				free(testImageLinear);
			}
			else//CLI(test image, ADS) => adjusted test image
			{
				testImageLinear = DSClinear;
				FILE* ADSFile = fopen(argv[2], "rb");
				//get number of bytes in file
				fseek(ADSFile, 0, SEEK_END);
				long size = ftell(ADSFile);
				rewind(ADSFile);//point ADSFile back to beginning
				double* ADS = (double*) malloc(size);
				fread(ADS, sizeof(double), size / sizeof(double), ADSFile);
				unsigned char* testImageAdjusted = (unsigned char*) malloc(length * width * sizeof(unsigned char));
				if(size != (width * length + 2) * sizeof(double) || applyADS(testImageLinear, testImageAdjusted, length, width, ADS))
				{
					fprintf(stderr, "ADS(%f %f) and test image(%d %d) dimensions mismatch\n", ADS[0], ADS[1], length, width);
					return 1;
				}
				free(testImageAdjusted);
				fclose(ADSFile);
				free(ADS);
			}
		}
		else
		{
			Pixel min;
			if(argc == 2)//CLI(DSC) => ADS
			{
				min = findTBT(DSC, length, width, lt);
			}
			else if(argc == 4)//CLI(DSC, x, y) => ADS
			{
				int x = atoi(argv[2]);
				int y = atoi(argv[3]);
				if(x < length && y < width && x >= 0 && y >= 0)
				{
					min = { x, y, DSC[x + y * length], DSClinear[x + y * length] };
				}
				else
				{
					fprintf(stderr, "pixel coordinates must be less than %d %d and greater than or equal to 0 0\n", length, width);
					return 1;
				}
			}
			else
			{
				fprintf(stderr, "invalid number of parameters\n");
				return 1;
			}
			double* ADS = (double*) malloc((width * length + 2) * sizeof(double));
			createADS(DSClinear, length, width, min, ADS);
			free(ADS);
		}
		free(DSClinear);
		stbi_image_free(DSC);
	}
	else
	{
		fprintf(stderr, "read readme.md on how to use\n");
		return 1;
	}
	return 0;
}