This program uses OpenGL 3, GLFW, and C++. It should compile on Windows, Linux, or Mac with

    make

This program will run in command line mode and write a file in the working directory with the name `fixed.png` when given 2 parameters(Test Image and Adjustment Data Structure)

    ./black_level_correction /path/to/test.png||jpg||jpeg path/to/adjustment

or write to `ADS` the offsets for each pixel when given 3 parameters(Dark Screen Calibration image and the x and y value of the pixel coordinate with the True Black Target)

    ./black_level_correction /path/to/DSC.png||jpg||jpeg x_coordinate_unsigned_integer y_coordinate_unsigned_integer

or write to `ADS` the offsets for each pixel when given 1 parameter(Dark Screen Calibration image). This mode will automatically determine the darkest True Black Target.

    ./black_level_correction /path/to/DSC.png||jpg||jpeg

or run in graphical user interface mode when 2 parameters(Dark screen Calibration Image and Test Image)

    ./black_level_correction /path/to/DSC.png||jpg||jpeg /path/to/test.png||jpg||jpeg

![Initial launch](screenshots/gui_launch.png "Initial launch")
![pick TBT](screenshots/picked_arbitrary_TBT.png "picked arbitrary TBT")
![test image subtract dsc](screenshots/subtract.png "test image - DSC")

All four windows with images are scrollable with a mouse. Clicking anywhere on the top left window will choose the `TBT`. The `ADS` file in the working directory gets updated after every action in the `GUI`. Sorry there is no file open dialog :(